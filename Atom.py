#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 29 14:23:47 2020

@author: Agata
"""

import copy

class Atom:
    
    __slots__ = ["__mass","__r","__v","__poz","__id"]
    
    def __init__(self,m,v,r,x,iid):
        self.__mass = m #float
        self.__v = copy.deepcopy(v) #kopia z vec2
        self.__r = r #float
        self.__poz = copy.deepcopy(x)#kopia z vec2
        self.__id = iid #id atomu
        
    def __str__(self):
        #return str(self.__id) + "[" + str(self.__poz.x) + " , " + str(self.__poz.y) + "]"
        return "%5.2f" % (self.__poz.x) + " "+ "%5.2f" % (self.__poz.y)
        
    @property #getter
    def mass(self):
        return self.__mass
    
    @mass.setter #setter
    def mass(self,new_mass):
        self.__mass = new_mass
        
    @property
    def v(self):
        return self.__v
    
    @v.setter
    def v(self,new_v):
        self.__v = copy.deepcopy(new_v)
       
    @property
    def r(self):
        return self.__r
    
    @r.setter
    def r(self,new_r):
        self.__r= new_r
        
    @property
    def poz(self):
        return self.__poz
    
    @poz.setter
    def poz(self,new_poz):
        self.__poz = copy.deepcopy(new_poz)
        
    @property
    def id(self):
        return self.__id

    @id.setter
    def id(self, new_id):
        self.__id = new_id
