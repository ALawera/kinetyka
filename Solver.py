#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 29 14:27:00 2020
@author: Agata
"""

from vec2 import Vec2
from Atom import Atom
from System import System
from Polygon import Polygon

class Solver:
    #dostanie system i bedzie ruszal atomami systemu
    def __init__(self,s):
        self.__at = s.atoms
        self.__sc = s.scene
        self.dt = 0.1
        self.tab=[]
        
    def point_on_segment_projection(self,beg,end,p):
       # print(beg," ",end)
        licznik = (p.x - beg.x) * (end.x - beg.x) + (p.y - beg.y) * (end.y - beg.y)
        mianownik = (end.distance_to_2(beg))
        t = licznik / mianownik
        pomocniczy = Vec2(beg.x + (t * (end.x - beg.x)) , beg.y + (t * (end.y - beg.y)))
        d = pomocniczy.distance_to(p)
        return t,d

    def kr_on(self,beg,end,atom):#kulka ruchoma odcinek nieruchomy(scianki)
        t,d = self.point_on_segment_projection(beg,end,atom.poz)
        if 0 <= t <= 1 :
            if d <= atom.r:
                x = t * (end.x - beg.x) + beg.x#skladowa x punktu (td)
                y = t * (end.y - beg.y) + beg.y#skladowa y punktu (td)
                wek_norm = Vec2(atom.poz.x-x,atom.poz.y-y)
                ilocz_skal = wek_norm.dot_product(atom.v)#iloczyn skalarny wektora predkosci i normalnego
                if ilocz_skal<0:
                    atom.v.x -= 2*((ilocz_skal/(wek_norm.length())**2) * wek_norm.x)
                    atom.v.y -= 2*((ilocz_skal/(wek_norm.length())**2) * wek_norm.y)
    
    def kr_kr(self,kulka1, kulka2):#kulka ruchoma kulka ruchoma
        sumapromieni = kulka1.r + kulka2.r
        srodko = Vec2(kulka1.poz.x - kulka2.poz.x,kulka1.poz.y - kulka2.poz.y) #pozycja srodkow
        R = srodko.length()
        if R <= sumapromieni:
            predkosci = Vec2(kulka1.v.x - kulka2.v.x,kulka1.v.y - kulka2.v.y)
            gora = predkosci.dot_product(srodko) #iloczyn skalarny wektorow
            dol = srodko.length()**2 #dlugosc wektora rodkow
            liczbe = gora/dol
            f = (2/(kulka1.mass + kulka2.mass)) * liczbe
            kulka1.v.x = kulka1.v.x - kulka2.mass * f * srodko.x
            kulka1.v.y = kulka1.v.y - kulka2.mass * f * srodko.y
            kulka2.v.x = kulka2.v.x + kulka1.mass * f * srodko.x
            kulka2.v.y = kulka2.v.y + kulka1.mass * f * srodko.y
    
    def run(self,zew,wew):#poruszanie sie obiektow
        for i in range(zew):
            for j in range(wew):
                for a in self.__at:
                    a.poz.x += a.v.x * self.dt
                    a.poz.y += a.v.y * self.dt
                    print(a, end ="\t")
                    self.tab.append(a)
                print("\n")
                for a in range(len(self.__at)):
                    for pol in self.__sc:
                        for ii in range(len(pol.nodes)):
                           # print(pol.nodes[i])
                            #print(pol.nodes[i%len(pol.nodes)])
                            self.kr_on(pol.nodes[ii],pol.nodes[(ii+1)%len(pol.nodes)],self.__at[a])
                    for kulk in self.__at[a+1:]:
                        self.kr_kr(self.__at[a],kulk)
                         
        
        
    #def odr_kr(self):
if __name__=='__main__':
    v1 = Vec2(1,1)
    x2 = Vec2(1,1)
    v3 = Vec2(-1,1)
    x4 = Vec2(3,1)
    N = 5
    p1 = Vec2(0,0)
    p2 = Vec2(0,N)
    p3 = Vec2(N,N)
    p4 = Vec2(N,0)
    kwadrat = Polygon(1,0,0,[p1,p2,p3,p4])
    a = Atom(1,v1,0.5,x2,0)
    a2 = Atom(1,v3,0.5,x4,1)
    s = System()
    s.add_to_scene(kwadrat)
    s.add_to_atom(a)
    s.add_to_atom(a2)
    solv = Solver(s)
    solv.run(10,10)
    
    
    