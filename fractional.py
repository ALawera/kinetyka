#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 24 16:14:41 2019

@author: Agata
"""
from polynomial import Polynomial
import copy

class Fractional:#ulamek
    
    __slots__ = ['__licz','__miano']
    
    def __init__(self, licz, miano):
        self.__licz = copy.deepcopy(licz)
        self.__miano = copy.deepcopy(miano)
        
    def __str__(self):#mowimy programowi jak ma nam to pokazac 
        s = str(self.__licz)+"/"+str(self.__miano)
        return s
        

    def __iadd__(self,xx):
        x = copy.deepcopy(xx)
        self.__licz *= x.__miano#a*d
        x.__licz *= self.__miano#b*c
        self.__licz += x.__licz#(a*d)+(b*c)
        self.__miano *= x.__miano#c*d
        return self
    
    def __isub__(self,xx):
        x = copy.deepcopy(xx)
        self.__licz *= x.__miano#a*d
        x.__licz *= self.__miano#b*c
        self.__licz -= x.__licz#(a*d)-(b*c)
        self.__miano *= x.__miano#c*d
        return self
    
    def __mul__(self,xx):
        x=copy.deepcopy(xx)
        self.__licz *= x.__licz
        self.__miano *= x.__miano
        return self
    
#WYWOLANIE ADD i SUB i MUL
#ul = Fractional(2,3)
#ul2= Fractional(3,4)
#ul*=ul2
#print(ul)

#RATIONAL (czyli mnozenie ulamkow wielomianowych)
w1 = Polynomial([1,1])
w2 = Polynomial([2,0,1])

a = Fractional(w1,w2)
b = Fractional(w2,w1)
c = Fractional(w1,w2)
a *= b
b += c
print(a)
print(b)
