
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 29 17:02:45 2020
@author: Agata
"""

import copy

class System:

    __slots__ = ['__scene','__atoms']

    def __init__(self):
        self.__scene=[]
        self.__atoms=[]

    def add_to_scene(self,o):
        self.__scene.append(copy.deepcopy(o))

    def add_to_atom(self,o):
        self.__atoms.append(copy.deepcopy(o))

    @property
    def atoms(self):
        return self.__atoms

    @property
    def scene(self):
        return self.__scene

    @property
    def objects(self):
        return self.__atoms.extend(self.__scene)


if __name__ == '__main__':
    s=System()

