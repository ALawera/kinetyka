#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 29 14:26:44 2020

@author: Agata
"""

from math import sqrt
import copy

class Vec2:
    
    __slots__ = ["__x","__y"]
    
    def __init__(self,x=0,y=0):
        self.__x = x
        self.__y = y
    
    def __str__(self):
        return "["+str(self.__x)+","+str(self.__y)+"]"
    
    def __iadd__(self,other):
        self.__x += other.__x
        self.__y += other.__y
        return self
    
    def __isub__(self,other):
        self.__x -= other.__x
        self.__y -= other.__y
        return self
    
    def __imul__(self,var):
        self.__x *= var
        self.__y *= var
        return self
    
    @property #getter
    def x(self):
        return self.__x

    @x.setter #setter (ustawia nowe wspolrzedne)
    def x(self,new_x):
        self.__x=new_x

    @property
    def y(self):
        return self.__y

    @y.setter
    def y(self, new_y):
        self.__y = new_y
    
    def dot_product(self,other):#iloczyn skalarny
        return self.__x*other.__x+self.__y*other.__y

    def normalize(self):
        self.__x/=self.length()
        self.__y/=self.length()

    def length(self):
        return sqrt(self.__x*self.__x+self.__y*self.__y)

    def angle(self,other):
        return self.dot_product(other) / (self.length() * other.length())

    def distance_to(self,other):
        return sqrt((other.__x-self.__x)*(other.__x-self.__x)+(other.__y-self.__y)*(other.__y-self.__y))

    def distance_to_2(self,other):
        return (other.__x-self.__x)*(other.__x-self.__x)+(other.__y-self.__y)*(other.__y-self.__y)
