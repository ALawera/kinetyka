#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 29 14:27:23 2020

@author: Agata
"""
from Atom import Atom
from vec2 import Vec2
import copy

#nodes(lista wierzcholkow vec2)

def center(node):#liczy srodek z wierzcholkow
    sumx = 0
    sumy = 0
    ilosc = len(node)
    for idd in range(len(node)):
        sumx += node[idd].x
        sumy += node[idd].y
    sr = Vec2 (sumx/ilosc,sumy/ilosc)
    r = 0
    for i in node :
        d = sr.distance_to(i)
        if d > r:
            r = d
            
    return r, sr
        
class Polygon(Atom):
    
    def __init__(self,m,v,iid,node):
        Atom.__init__(self,m,v,center(node)[0],center(node)[1],iid)
        self.nodes = copy.deepcopy(node)
        #for i in self.nodes:
         #   print(i)
        